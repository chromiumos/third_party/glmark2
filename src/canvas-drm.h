/*
 * Copyright © 2010-2012 Linaro Limited
 *
 * This file is part of the glmark2 OpenGL (ES) 2.0 benchmark.
 *
 * glmark2 is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * glmark2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * glmark2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *  Alexandros Frantzis (glmark2)
 *  Simon Que
 */
#ifndef GLMARK2_CANVAS_DRM_H_
#define GLMARK2_CANVAS_DRM_H_

#include "canvas.h"

#define EGL_EGLEXT_PROTOTYPES
#define GL_GLEXT_PROTOTYPES
#define MESA_EGL_NO_X11_HEADERS

#include "gl-headers.h"

#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <gbm.h>
#include <drm.h>
#include <xf86drm.h>
#include <xf86drmMode.h>

#include <vector>
#include <queue>
/**
 * Canvas for direct rendering with EGL.
 */
class CanvasDRM: public Canvas
{
public:
    CanvasDRM(int width, int height) :
        Canvas(width, height) {}
    ~CanvasDRM();

    virtual bool init();
    virtual bool reset();
    virtual void visible(bool visible);
    virtual void clear();
    virtual void update();
    virtual void print_info();
    virtual Pixel read_pixel(int x, int y);
    virtual void write_to_file(std::string &filename);
    virtual bool should_quit();
    virtual void resize(int width, int height);
    virtual int get_frame_buffer();

protected:
    virtual bool make_current();
    virtual bool reset_context();
    virtual void swap_buffers();
    virtual bool supports_gl2();

private:
    struct kms {
        drmModeConnector *connector;
        drmModeEncoder *encoder;
        drmModeModeInfo mode;
        std::vector<uint32_t> fb_ids;
    };
    struct page_flip_data
    {
        CanvasDRM* canvas;
        int buffer_index;
    };

    EGLBoolean setup_kms(int fd_, struct kms *kms_);

    void resize_no_viewport(int width, int height);

    bool ensure_egl_display();
    bool ensure_egl_config();
    bool ensure_egl_context();
    bool ensure_egl_surface();
    void init_gl_extensions();
    static void page_flip_handler(int fd, unsigned int frame, unsigned int sec,
                                  unsigned int usec, void* data);

    static void quit_handler(int signum);

    EGLDisplay egl_display_;
    EGLSurface egl_surface_;
    EGLConfig egl_config_;
    EGLContext egl_context_;

    int num_buffers_;
    std::vector<EGLImageKHR> images_;
    GLuint fb_, depth_rb_;
    std::vector<GLuint> render_buffers_;
    struct kms kms_;
    int fd_;
    struct gbm_device *gbm_;
    std::vector<struct gbm_bo *> buffer_objs_;
    drmModeCrtcPtr saved_crtc_;

    static bool should_quit_;

    int current_back_buffer_index_;
    int current_front_buffer_index_;
    std::queue<int> available_buffers_;
    std::queue<int> rendered_buffers_;
};

#endif
