/*
 * Copyright © 2010-2011 Linaro Limited
 *
 * This file is part of the glmark2 OpenGL (ES) 2.0 benchmark.
 *
 * glmark2 is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * glmark2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * glmark2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *  Alexandros Frantzis (glmark2)
 */
#ifndef GLMARK2_GL_HEADERS_H_
#define GLMARK2_GL_HEADERS_H_

#define GL_GLEXT_PROTOTYPES

#if USE_GL
#include <GL/gl.h>
#include <GL/glext.h>
#elif USE_GLESv2
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#ifndef GL_WRITE_ONLY
#define GL_WRITE_ONLY GL_WRITE_ONLY_OES
#endif
#endif

/**
 * Struct that holds pointers to functions that belong to extensions
 * in either GL2.0 or GLES2.0.
 */
struct GLExtensions {
    static void* (*MapBuffer) (GLenum target, GLenum access);
    static GLboolean (*UnmapBuffer) (GLenum target);
};

#endif
