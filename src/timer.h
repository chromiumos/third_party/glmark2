/*
 * Copyright © 2012 Linaro Limited
 *
 * This file is part of glmark2.
 *
 * glmark2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * glmark2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with glmark2.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *  Simon Que <sque@chromium.org>
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <map>
#include <string>
#include <vector>

#include <stdint.h>

/*
 * Timer class -- used for measuring time taken between lines of code.
 *
 * Usage example:
 *
 * In top-level function:
 *    Timer timer(100, "MyTimer");  // Record up to 100 loops, name it "MyTimer"
 *
 *    while (running) {
 *      timer.get_timestamp(__LINE__);
 *
 *      // Some code.
 *
 *      timer.get_timestamp(__LINE__);
 *
 *      // Call a function.
 *      some_function();
 *
 *      timer.get_timestamp(__LINE__);
 *      timer.next_cycle();
 *    }
 *
 *  timer.print_results();
 *
 * In function some_function():
 *
 *    void some_function() {
 *      Timer* timer = Timer::get_timer("MyTimer");
 *
 *      // Some code.
 *
 *      timer->get_timestamp(__LINE__);
 *
 *      // Some more code.
 *    }
 */

class Timer {
  public:
    Timer(int max_cycles, const std::string &name);
    ~Timer();
    void set_max_timestamps(int max_timestamps);
    void next_cycle();
    void read_timestamp(int id);
    void print_results();

    static Timer* get_timer(const std::string& name);
  private:
    std::vector<std::vector<uint64_t> > data_;
    std::vector<uint64_t>* current_cycle_data_;
    std::vector<int> timestamp_ids_;
    int cycle_count_, timestamp_count_;
    int max_cycles_, max_timestamps_;

    std::string name_;
    static std::map<std::string, Timer*> timer_list;
};

#endif /* TIMER_H_ */
